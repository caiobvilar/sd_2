﻿#include <iostream>
#include <pthread.h>
//#include <math.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include "BlackGPIO/BlackGPIO.h"
#include "ADC/Adc.h"

#define UNIT_MS 1000;
#define UNIT_SEC 1000000;

using namespace std;

/**
  7 Segments display

       2
   1       3
       7
   6       4
       5
*/

int led_on;
BlackLib::BlackGPIO *d1;
BlackLib::BlackGPIO *d2;
BlackLib::BlackGPIO *d3;
BlackLib::BlackGPIO *d4;
BlackLib::BlackGPIO *d5;
BlackLib::BlackGPIO *d6;
BlackLib::BlackGPIO *d7;

struct timeval tempo_inicial, tempo_final;

void show_result(int result) {
    cout << "Result = " << result << endl;

    //d1->setValue(BlackLib::high);

    if (result < 0) {
        d1->setValue(BlackLib::low);
        d2->setValue(BlackLib::low);
        d3->setValue(BlackLib::low);
        d4->setValue(BlackLib::low);
        d5->setValue(BlackLib::low);
        d6->setValue(BlackLib::low);
        d7->setValue(BlackLib::low);

        return;
    }

    if (result != 1 && result != 2 && result != 3 && result != 7) {
        d1->setValue(BlackLib::high);
    } else {
        d1->setValue(BlackLib::low);
    }

    if (result != 1 && result != 4) {
        d2->setValue(BlackLib::high);
    } else {
        d2->setValue(BlackLib::low);
    }

    if (result != 5 && result != 6) {
        d3->setValue(BlackLib::high);
    } else {
        d3->setValue(BlackLib::low);
    }

    if (result != 2) {
        d4->setValue(BlackLib::high);
    } else {
        d4->setValue(BlackLib::low);
    }

    if (result != 1 && result != 4 && result != 7) {
        d5->setValue(BlackLib::high);
    } else {
        d5->setValue(BlackLib::low);
    }

    if (result != 1 && result != 3 && result != 4 && result != 5 && result != 7) {
        d6->setValue(BlackLib::high);
    } else {
        d6->setValue(BlackLib::low);
    }

    if (result != 0 && result != 1 && result != 7) {
        d7->setValue(BlackLib::high);
    } else {
        d7->setValue(BlackLib::low);
    }

    if (result > 9) {
        // E
        d3->setValue(BlackLib::low);
        d4->setValue(BlackLib::low);
    }
}

void write_usr0_led(const char *value) {
    cout << "LED change" << endl;

    const char *LEDBrightness = "/sys/class/leds/beaglebone:green:usr0/brightness";
    FILE *LEDHandle = NULL;

    if ((LEDHandle = fopen(LEDBrightness, "r+")) != NULL) {
        fwrite(value, sizeof(char), 1, LEDHandle);
        fclose(LEDHandle);
    }

    cout << "LED change end" << endl;
}

void switch_off_led(BlackLib::BlackGPIO &led) {
    //write_usr0_led("0");
    led.setValue(BlackLib::low);
}

void switch_on_led(BlackLib::BlackGPIO &led) {
    //write_usr0_led("1");
    led.setValue(BlackLib::high);



    //timeval tempo_final;
    gettimeofday(&tempo_inicial, NULL);
    led_on = 1;
    //led_on = (time.tv_sec * 1000) + (time.tv_usec / 1000);
    //led_on = (int) (1000 * (tempo_final.tv_sec - tempo_inicial.tv_sec) + (tempo_final.tv_usec - tempo_inicial.tv_usec) / 1000);

    //cout << "led_on: " << led_on << endl;

    //led_on = time(NULL);
}

void blink_led(BlackLib::BlackGPIO &led) {
    switch_on_led(led);
    usleep(400000); // 400 ms
    switch_off_led(led);
}

int roll(int min, int max)
{
    int range = max - min + 1;
    return rand() % range + min;
}

void* button_press_monitor(void * nothing) {
    BlackLib::BlackGPIO button(BlackLib::GPIO_44,BlackLib::input, BlackLib::SecureMode);

    while (true) {
        //int rand = roll(0, 1);

        if (button.getValue().compare("1") == 0) {
            if (led_on > 0) {
                //timeval time;
                //gettimeofday(&time, NULL);
                //int current = (time.tv_sec * 1000) + (time.tv_usec / 1000);

                //cout << "led_on: " << led_on << endl;
                //cout << "atual: " << current << endl;

                // user preesed button after blink, show tim at monitor
                //int result = ((current - led_on) % 10000);

                gettimeofday(&tempo_final, NULL);
                int result = (int) (1000 * (tempo_final.tv_sec - tempo_inicial.tv_sec) + (tempo_final.tv_usec - tempo_inicial.tv_usec) / 1000); // para transformar em milissegundos

                cout << "result: " << result << endl;

                show_result( (result % 1000) / 100);

                led_on = 0;
            }
        }

    }
}

int main(int argc, char * argv[])
{
    cout << "Atividade 1" << endl;
    cout << "Pressione o botão quando o LED acender." << endl;

    pthread_t bt_thread;
    pthread_create(&bt_thread, NULL, &button_press_monitor, NULL);

    //const char *LEDBrightness = "/sys/class/leds/beaglebone:green:usr0/brightness";
    //FILE *LEDHandle = NULL;


    d1 = new BlackLib::BlackGPIO(BlackLib::GPIO_66,BlackLib::output, BlackLib::SecureMode);
    d2 = new BlackLib::BlackGPIO(BlackLib::GPIO_69,BlackLib::output, BlackLib::SecureMode);
    d3 = new BlackLib::BlackGPIO(BlackLib::GPIO_45,BlackLib::output, BlackLib::SecureMode);
    d4 = new BlackLib::BlackGPIO(BlackLib::GPIO_23,BlackLib::output, BlackLib::SecureMode);
    d5 = new BlackLib::BlackGPIO(BlackLib::GPIO_47,BlackLib::output, BlackLib::SecureMode);
    d6 = new BlackLib::BlackGPIO(BlackLib::GPIO_27,BlackLib::output, BlackLib::SecureMode);
    d7 = new BlackLib::BlackGPIO(BlackLib::GPIO_22,BlackLib::output, BlackLib::SecureMode);

    show_result(-1);

    BlackLib::BlackGPIO led(BlackLib::GPIO_67,BlackLib::output, BlackLib::SecureMode);
    srand(time(NULL));

    led.setValue(BlackLib::low);

    while (true) {
        //long tempo = 3*UNIT_SEC;
        //led.setValue(BlackLib::high);
        //usleep(tempo);
        //led.setValue(BlackLib::low);
        //usleep(tempo);

        cout << "-> loop begin--> " << endl;

        int delay = roll(1000, 3000);

        cout << "Delay (ms): " << (delay) << endl;

        usleep(delay * 1000);

        blink_led(led);
        //led.setValue(BlackLib::low);

        cout << "-> loop end " << endl;

        sleep(5); // max time to user press button
        led_on = 0;

        show_result(-1);
    }

    return 0;
}
